package com.example.institucionalempresa;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends Activity {


    private ImageButton BotaoEmpresa;
    private ImageButton BotaoServicos;
    private ImageButton BotaoCliente;
    private ImageButton BotaoContatos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BotaoEmpresa = findViewById(R.id.EmpresaBotaoID);
        BotaoServicos = findViewById(R.id.ServicosBotaoID);
        BotaoCliente = findViewById(R.id.ClienteBotaoID);
        BotaoContatos = findViewById(R.id.ContatoBotaoID);

        BotaoEmpresa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this , ActivityEmpresa.class));
            }
        });

        BotaoServicos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this , ActivityServicos.class));
            }

        });

        BotaoCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ActivityCliente.class));
            }
        });

        BotaoContatos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this , ActivityContatos.class));
            }
        });
    }
}
